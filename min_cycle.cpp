/*
 * Задача 2. Цикл минимальной длины
 * Дан невзвешенный неориентированный граф. Найдите цикл минимальной длины.
 * Время - O(n(n+m)), память - O(n+m)
 */
#pragma GCC optimize("Ofast")
#include <iostream>
#include <list>
#include <queue>
#include <vector>

using std::list;
using std::queue;
using std::vector;

struct IGraph {
  virtual ~IGraph() {}

  virtual void AddEdge(int from, int to) = 0;

  virtual int VerticesCount() const = 0;

  virtual void GetNextVertices(int vertex,
                               std::vector<int>& vertices) const = 0;
  virtual void GetPrevVertices(int vertex,
                               std::vector<int>& vertices) const = 0;
};

struct ListGraph : public IGraph {
 private:
  std::vector<std::list<int> > adjacency_list;
  std::vector<std::list<int> > reverse_adjacency_list;

 public:
  ListGraph(int n) : adjacency_list(n), reverse_adjacency_list(n) {}
  ~ListGraph() = default;
  ListGraph(const IGraph* graph);
  void AddEdge(int from, int to) override;
  int VerticesCount() const override;
  void GetNextVertices(int vertex, std::vector<int>& vertices) const override;
  void GetPrevVertices(int vertex, std::vector<int>& vertices) const override;
};

int getMinCycle(const IGraph& graph);

int main() {
  std::ios_base::sync_with_stdio(false);
  int vertices, edges;
  std::cin >> vertices >> edges;

  ListGraph graph(vertices);
  for (int i = 0; i < edges; i++) {
    int begin, end;
    std::cin >> begin >> end;
    graph.AddEdge(begin, end);
    graph.AddEdge(end, begin);
  }
  std::cout << getMinCycle(graph);
  return 0;
}

int getMinCycle(const IGraph& graph) {
  size_t verticesCount = graph.VerticesCount();
  size_t currentAnswer = verticesCount + 1;
  for (size_t vertex = 0; vertex < verticesCount; vertex++) { // Запускаемся от всех вершин
    vector<size_t> distances(verticesCount);
    vector<char> visited(verticesCount);
    queue<int> current_queue;
    vector<int> parents(verticesCount);
    parents[vertex] = -1;
    visited[vertex] = true;
    current_queue.push(vertex);
    while (!current_queue.empty()) {
      int curVertex = current_queue.front();
      current_queue.pop();
      vector<int> nextVert;
      graph.GetNextVertices(curVertex, nextVert);
      for (auto neigh : nextVert) {
        if (visited[neigh] && neigh != parents[curVertex]) { // Нашли цикл с рассматриваемой вершиной
          currentAnswer = std::min(currentAnswer,
                                   distances[neigh] + distances[curVertex] + 1);
          if (currentAnswer == 3) return currentAnswer; // Эвристика из-за которой заходит в TL
        }
        if (!visited[neigh]) {
          visited[neigh] = true;
          distances[neigh] = distances[curVertex] + 1;
          parents[neigh] = curVertex;
          current_queue.push(neigh);
        }
      }
    }
  }
  if (currentAnswer == verticesCount + 1)
    return -1;
  else
    return currentAnswer;
}

void ListGraph::AddEdge(int from, int to) {
  ListGraph::adjacency_list[from].push_front(to);
  ListGraph::reverse_adjacency_list[to].push_front(from);
}

int ListGraph::VerticesCount() const {
  return ListGraph::adjacency_list.size();
}

void ListGraph::GetNextVertices(int vertex, vector<int>& vertices) const {
  for (auto it : ListGraph::adjacency_list[vertex]) {
    vertices.push_back(it);
  }
}

void ListGraph::GetPrevVertices(int vertex, vector<int>& vertices) const {
  for (auto it : ListGraph::reverse_adjacency_list[vertex]) {
    vertices.push_back(it);
  }
}

ListGraph::ListGraph(const IGraph* graph)
    : adjacency_list(graph->VerticesCount()) {
  vector<int> temp;
  for (int i = 0; i < adjacency_list.size(); i++) {
    temp.clear();
    graph->GetNextVertices(i, temp);
    adjacency_list[i].insert(adjacency_list[i].end(), temp.begin(), temp.end());
  }
}
